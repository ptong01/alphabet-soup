package org.preston.alphabet_soup;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;

/**
 * Main runner class that accepts file input and passes
 * parsed data to WordSearch class. Found results are
 * output to system.
 */
public class Main {
	public static void main(String [] args) {

		// Letters buffer to be passed to WordSearch class
		List<List<String>> letters = new ArrayList<List<String>>();  

		try {
			// File Selector
			JFileChooser chooser = new JFileChooser();

			int status = chooser.showOpenDialog(null);
			if (status == JFileChooser.APPROVE_OPTION) {
				File input = chooser.getSelectedFile();

				// Return if file not found
				if (input == null) {
					return;
				}


				// Load file into scanner
				Scanner scanner = new Scanner(input);

				// Parse first line for height and width
				String dimensions = scanner.nextLine();
				int height = Integer.parseInt(dimensions.split("x")[0]);
				int width = Integer.parseInt(dimensions.split("x")[1]);

				// Fill letters buffer with file contents
				for(int i=0; i < height; i++) {
					String row = scanner.nextLine();
					List<String> rowList = Arrays.asList(row.split(" "));
					letters.add(rowList);
				}

				// Create WordSearch class 
				WordSearch wordSearch = new WordSearch(height, width, letters);

				// Query WordSearch class
				while (scanner.hasNextLine()) {
					String query = scanner.nextLine();
					Result res = wordSearch.search(query.replace(" ", "")); // Remove any spaces in queries

					// Query found
					if(res.isFound()) {
						System.out.println(res.toString());
					}
				}
				scanner.close();
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.printStackTrace();
		}
	}



}

