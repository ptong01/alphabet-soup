package org.preston.alphabet_soup;


/**
 * Result class to organize found data.
 */
public class Result {

	private String query;
	private int startRowIndex;
	private int startColumnIndex;
	private int endRowIndex;
	private int endColumnIndex;
	private boolean found;

	/**
	 * WordSearch Constructor
	 *
	 * @param found Boolean if result was discovered
	 */
	public Result(boolean found) {
		this.found = found;
	}

	/**
	 * WordSearch Constructor
	 *
	 * @param query             String to find
	 * @param startRowIndex     Starting X coordinate of found word
	 * @param startColumnIndex  Starting Y coordinate of found word
	 * @param endRowIndex       Ending X coordinate of found word
	 * @param endColumnIndex    Ending Y coordinate of found word
	 * @param found             Boolean if result was discovered
	 */
	public Result(String query, int startRowIndex, int startColumnIndex, int endRowIndex, int endColumnIndex, boolean found) {
		this.query = query;
		this.startRowIndex = startRowIndex;
		this.startColumnIndex = startColumnIndex;
		this.endRowIndex = endRowIndex;
		this.endColumnIndex = endColumnIndex;
		this.found = found;
	}


	public boolean isFound() {
		return found;
	}

	public String toString() {
		return String.format("%s %d:%d %d:%d", query, startRowIndex, startColumnIndex, endRowIndex, endColumnIndex); 
	}
}
