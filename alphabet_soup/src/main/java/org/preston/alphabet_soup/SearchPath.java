package org.preston.alphabet_soup;

/**
 * SearchPath class denoting the direction to perform
 * a search crawl
 */
public class SearchPath {

	private String name;
	private int rowMod;
	private int columnMod;

	/**
	 * SearchPath Constructor
	 *
	 * @param name      Name describing the direction of path.
	 * @param rowMod    Row Modifier indicating the direction to crawl in the vertical orientation
	 * @param columnMod Column Modifier indicating the direction to crawl in the horizontal orientation
	 */
	public SearchPath(String name, int rowMod, int columnMod) {
		this.name = name;
		this.rowMod = rowMod;
		this.columnMod = columnMod;
	}

	public String getName() {
		return name;
	}

	public int getRowMod() {
		return rowMod;
	}

	public int getColumnMod() {
		return columnMod;
	}



}
