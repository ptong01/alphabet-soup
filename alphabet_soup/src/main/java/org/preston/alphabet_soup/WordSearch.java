package org.preston.alphabet_soup;

import java.util.Arrays;
import java.util.List;

/**
 * The WordSearch class contains functionality to search for desired queries.
 * Queries may be found on the puzzle's row, column, and diagonal.
 * Found queries may also appear forwards or backwards. This solution limits the
 * potential search paths to those that will not exceed the word searches boundaries. 
 * For example, a 5x5 WordSearch query of 4 length will not perform any
 * row or diagonal searches on the letter in the first row, third column but will
 * perform a column search.
 */
public class WordSearch {

	private int height;
	private int width;
	private List<List<String>> letters;

	// Potential search paths to crawl. We only need these four because we also check
	// for reversed strings, which accounts for the other four directions and saves on
	// computation.
	private static final List<SearchPath> SEARCH_PATHS = Arrays.asList(new SearchPath("RIGHT", 0, 1),
			new SearchPath("DOWN", 1, 0), new SearchPath("LEFT_DIAGONAL", 1, -1),
			new SearchPath("RIGHT_DIAGONAL", 1, 1));

	/**
	 * WordSearch Constructor
	 *
	 * @param height  Height of the WordSearch
	 * @param width   Width of the WordSearch
	 * @param letters 2D List of Lists storing letters
	 */
	public WordSearch(int height, int width, List<List<String>> letters) {
		this.height = height;
		this.width = width;
		this.letters = letters;
	}

	/**
	 * Helper method to reverse a given string
	 *
	 * @param str     String to be reversed
	 * @return        Newly reversed string
	 */
	private String reverseString(String str) {
		return new StringBuilder(str).reverse().toString();
	}


	/**
	 * Crawl a search path for a query and look for any matches
	 *
	 * @param query       String to find
	 * @param rowIndex    Row position of letter
	 * @param columnIndex Column position of letter
	 * @param path        Search path that contains the modifying direction to crawl 
	 * @return Result     Object containing the query in addition to the starting and
	 *         ending position of found query.
	 */
	private Result crawlPath(String query, int rowIndex, int columnIndex, SearchPath path) {
		String result = "";
		int endRowIndex = 0;
		int endColumnIndex = 0;


		// Increment the row and column indices based on the given path modifier directions
		for (int i = rowIndex, j = columnIndex; i < rowIndex + query.length() && j < columnIndex + query.length(); i = i + path.getRowMod(), j = j + path.getColumnMod()) {

			// Append letter to result
			result += letters.get(i).get(j); 

			// Easy way to get the ending row and column indices that the for loop ends on
			endRowIndex = i;
			endColumnIndex = j;
		}


		// Check if the result matches the query. Return a result if found.
		if (result.equals(query)) { 
			return new Result(query, rowIndex, columnIndex, endRowIndex, endColumnIndex, true); 
		}

		// Check if the result matches the reversed queries to account for backward results. Return a result if found.
		if (result.equals(reverseString(query))) { 
			return new Result(query, endRowIndex, endColumnIndex, rowIndex, columnIndex, true); 
		}

		// Result not found
		return new Result(false); 
	}

	/**
	 * Search for a given query in the word search
	 *
	 * @param query     String to be found
	 * @return Result   Object containing the query in addition to the starting and
	 *         ending position of found query.
	 */
	public Result search(String query) {

		// Iterate over all letters in word search
		for (int rowIndex = 0; rowIndex < height; rowIndex++) {
			for (int columnIndex = 0; columnIndex < width; columnIndex++) {

				// Iterate over all search paths
				for (SearchPath path : SEARCH_PATHS) {

					int searchLength = query.length() - 1;

					// Do not perform searches on a path if the crawl will go out of boundaries of the word search
					// This check saves on performance by pruning unneeded crawls
					if (rowIndex + (path.getRowMod() * searchLength) >= 0
							&& rowIndex + (path.getRowMod() * searchLength) < height
							&& columnIndex + (path.getColumnMod() * searchLength) >= 0
							&& columnIndex + (path.getColumnMod() * searchLength) < width) {

						// Crawl path from given point
						Result res = crawlPath(query, rowIndex, columnIndex, path);

						// Return result if found
						if (res.isFound()) {
							return res;
						}

					}
				}
			}
		}

		// Result not found
		return new Result(false);
	}

}
