package org.preston.alphabet_soup;


import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class WordSearchTest 
{

	private static final List<List<String>> FIRST_EXAMPLE = Arrays.asList(
			Arrays.asList("A", "B", "C"),
			Arrays.asList("D", "E", "F"),
			Arrays.asList("G", "H", "I"));


	private static final List<List<String>> SECOND_EXAMPLE = Arrays.asList(
			Arrays.asList("H", "A", "S", "D", "F"),
			Arrays.asList("G", "E", "Y", "B", "H"),
			Arrays.asList("J", "K", "L", "Z", "X"),
			Arrays.asList("C", "V", "B", "L", "N"),
			Arrays.asList("G", "O", "O", "D", "O"));

	private static final List<List<String>> CUSTOM_EXAMPLE = Arrays.asList(
			Arrays.asList("H", "A", "S"),
			Arrays.asList("G", "E", "Y"),
			Arrays.asList("J", "K", "L"),
			Arrays.asList("C", "V", "B"),
			Arrays.asList("G", "O", "O"));
	
	private static final List<List<String>> CUSTOM_EXAMPLE_2 = Arrays.asList(
			Arrays.asList("A"));


	private String query;
	private String expectedResult;
	private boolean found;

	private WordSearch wordSearch;

	public WordSearchTest(int height, int width, List<List<String>> letters, String query, String expectedResult, boolean found) {
		wordSearch = new WordSearch(height, width, letters);
		this.query = query;
		this.expectedResult = expectedResult;
		this.found = found;
	}


	@Parameterized.Parameters
	public static Collection givenExamples() {
		return Arrays.asList(new Object[][] {
			// Given example tests
			{ 3, 3, FIRST_EXAMPLE, "ABC", "ABC 0:0 0:2", true },
			{ 3, 3, FIRST_EXAMPLE, "AEI", "AEI 0:0 2:2", true },
			{ 5, 5, SECOND_EXAMPLE, "HELLO", "HELLO 0:0 4:4", true },
			{ 5, 5, SECOND_EXAMPLE, "GOOD", "GOOD 4:0 4:3", true },
			{ 5, 5, SECOND_EXAMPLE, "BYE", "BYE 1:3 1:1", true },

			// Test not found
			{ 3, 3, FIRST_EXAMPLE, "AAA", null, false },
			{ 5, 5, SECOND_EXAMPLE, "HASE", null, false },

			// Custom tests
			{ 5, 3, CUSTOM_EXAMPLE, "CO", "CO 3:0 4:1", true }, // Right Diagonal
			{ 5, 3, CUSTOM_EXAMPLE, "OC", "OC 4:1 3:0", true }, // Reverse Right Diagonal
			{ 5, 3, CUSTOM_EXAMPLE, "OBLY", "OBLY 4:2 1:2", true }, // Up
			{ 5, 3, CUSTOM_EXAMPLE, "LKJ", "LKJ 2:2 2:0", true }, // Left
			{ 5, 3, CUSTOM_EXAMPLE, "VG", "VG 3:1 4:0", true }, // Left Diagonal
			{ 5, 3, CUSTOM_EXAMPLE, "GV", "GV 4:0 3:1", true }, // Reverse Left Diagonal
			{ 5, 3, CUSTOM_EXAMPLE, "LONG", null, false }, // Missing Long String
			{ 1, 1, CUSTOM_EXAMPLE_2, "A", "A 0:0 0:0", true }, // Single Letter
			{ 1, 1, CUSTOM_EXAMPLE_2, "B", null, false }, // Missing Letter
			{ 1, 1, CUSTOM_EXAMPLE_2, "AB", null, false }, // Missing Out of Bounds
		});
	}

	@Test
	public void TestExamples()
	{
		Result res = wordSearch.search(query);

		// Check if result is found
		assertTrue( res.isFound() == found );

		// If found then check if result matches expected result
		if (res.isFound()) {
			assertTrue( res.toString().equals(expectedResult) );
		}

	}



}
