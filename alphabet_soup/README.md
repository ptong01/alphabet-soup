# Alphabet Soup

Alphabet Soup is a word search puzzle solver that accepts a file input and outputs results
to system.

This solution optimizes performance from brute force in two aspects:
1. Pruning potential paths to crawl that will result in out of bound searches.
2. Comparing queries to reversed results in addition to the original string to save on
expensive computation that comes from path crawls. By doing this, we only have to perform
crawls in four directions as opposed to eight.


## Prerequisites

- Java
- Maven

## Build

Clone the Repo
`git clone https://gitlab.com/ptong01/alphabet-soup`

The project can be built through either Eclipse or command line

Eclipse
- Right click `pom.xml`
- `Run as`
- `Maven build`
- Enter `package` in goals if prompted

Command Line
```
mvn package
```

## Run

In a terminal:

```
java -jar target/alphabet_soup-0.0.1-SNAPSHOT.jar
```